﻿using Microsoft.Extensions.Logging;
using Sales.Api.Borders.Repositories;
using Sales.Api.Borders.UseCases;
using System;
using Entities = Sales.Api.Borders.Entities;

namespace Sales.Api.UseCases.Sale
{
    public class GetSaleUseCase : IGetSaleUseCase
    {
        private readonly ILogger<GetSaleUseCase> _logger;
        private readonly ISalesRepository _salesRepository;

        public GetSaleUseCase(ILogger<GetSaleUseCase> logger, ISalesRepository salesRepository)
        {
            _logger = logger;
            _salesRepository = salesRepository;
        }

        public Entities.Sale Execute(Guid saleId)
        {
            _logger.LogInformation("GetSaleUseCase running...");

            var sale = _salesRepository.GetSale(saleId);
            if (sale != null)
            {
                return sale;
            }

            throw new Exception("Sale not found!");
        }
    }
}