﻿using Microsoft.Extensions.Logging;
using Sales.Api.Borders.Repositories;
using Sales.Api.Borders.UseCases;
using System;
using Entities = Sales.Api.Borders.Entities;

namespace Sales.Api.UseCases.Sale
{
    public class UpdateSaleUseCase : IUpdateSaleUseCase
    {
        private readonly ILogger<GetSaleUseCase> _logger;
        private readonly ISalesRepository _salesRepository;

        public UpdateSaleUseCase(ILogger<GetSaleUseCase> logger, ISalesRepository salesRepository)
        {
            _logger = logger;
            _salesRepository = salesRepository;
        }

        public Entities.Sale Execute(Guid saleId, Entities.Sale sale)
        {
            _logger.LogInformation("GetSaleUseCase running...");

            var saleToUpdate = _salesRepository.GetSale(saleId);
            if (saleToUpdate != null)
            {
                saleToUpdate.Status = sale.Status;
                return _salesRepository.UpdateSale(sale);
            }

            throw new Exception("Sale not found!");
        }
    }
}