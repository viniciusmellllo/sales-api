﻿using Microsoft.Extensions.Logging;
using Sales.Api.Borders.Dtos;
using Sales.Api.Borders.Enums;
using Sales.Api.Borders.Repositories;
using Sales.Api.Borders.UseCases;
using System;
using Entities = Sales.Api.Borders.Entities;

namespace Sales.Api.UseCases.Sale
{
    public class CreateSaleUseCase : ICreateSaleUseCase
    {
        private readonly ILogger<GetSaleUseCase> _logger;
        private readonly ISalesRepository _salesRepository;

        public CreateSaleUseCase(ILogger<GetSaleUseCase> logger, ISalesRepository salesRepository)
        {
            _logger = logger;
            _salesRepository = salesRepository;
        }

        public Entities.Sale Execute(SaleRequest saleRequest)
        {
            _logger.LogInformation("GetSaleUseCase running...");

            if (saleRequest.Items == null || saleRequest.Items.Count <= 0)
            {
                throw new Exception("Sale must have at least one item!");
            }

            var sale = new Entities.Sale
            {
                Id = Guid.NewGuid(),
                Items = saleRequest.Items,
                Salesman = saleRequest.Salesman,
                SoldAt = DateTime.Now,
                OrderNumber = (new Random()).Next(100),
                Status = SaleStatus.AcceptedPayment
            };

            return _salesRepository.CreateSale(sale);
        }
    }
}