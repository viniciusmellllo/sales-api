﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sales.Api.Borders.Dtos;
using Sales.Api.Borders.Entities;
using Sales.Api.Borders.UseCases;
using System;
using System.Net;

namespace Sales.Api.Controllers
{
    [ApiController]
    [Route("api/sales")]
    public class SalesController : ControllerBase
    {
        private readonly ILogger<SalesController> _logger;
        private readonly IGetSaleUseCase _getSaleUseCase;
        private readonly ICreateSaleUseCase _createSaleUseCase;
        private readonly IUpdateSaleUseCase _updateSaleUseCase;

        public SalesController(
            ILogger<SalesController> logger,
            IGetSaleUseCase getSaleUseCase,
            ICreateSaleUseCase createSaleUseCase,
            IUpdateSaleUseCase updateSaleUseCase
        )
        {
            _logger = logger;
            _getSaleUseCase = getSaleUseCase;
            _createSaleUseCase = createSaleUseCase;
            _updateSaleUseCase = updateSaleUseCase;
        }

        [HttpGet("{saleId}")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Sale))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(string))]
        public Sale Get(Guid saleId)
        {
            _logger.LogInformation("Get method called...");
            return _getSaleUseCase.Execute(saleId);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(Sale))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
        public Sale Create([FromBody] SaleRequest saleRequest)
        {
            _logger.LogInformation("Create method called...");
            return _createSaleUseCase.Execute(saleRequest);
        }

        [HttpPut("{saleId}")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Sale))]
        [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(string))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
        public Sale Update(Guid saleId, [FromBody] Sale sale)
        {
            _logger.LogInformation("Update method called...");
            return _updateSaleUseCase.Execute(saleId, sale);
        }
    }
}