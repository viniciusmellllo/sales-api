﻿using Microsoft.Extensions.DependencyInjection;
using Sales.Api.Shared.Util;
using Sales.Api.UseCases.Sale;
using System;
using System.Linq;

namespace Sales.Api.Configuration
{
    public static class UseCaseConfig
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            Type classRefType = typeof(GetSaleUseCase);
            //Here we want to get First or throw an InvalidOperationException, not FirstOrDefault.
            Type interfaceRefType = classRefType.GetInterfaces().First(it => it.Name == "I" + classRefType.Name);

            ReflectionUtils.ForEachInterfaceClass(interfaceRefType, classRefType, delegate (Type it, Type ct)
            {
                services.AddScoped(it, ct);
            }, true, true);
        }
    }
}