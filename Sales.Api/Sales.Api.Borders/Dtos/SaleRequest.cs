﻿using Sales.Api.Borders.Entities;
using System.Collections.Generic;

namespace Sales.Api.Borders.Dtos
{
    public class SaleRequest
    {
        public Salesman Salesman { get; set; }
        public List<Item> Items { get; set; }
    }
}