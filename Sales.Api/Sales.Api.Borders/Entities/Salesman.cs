﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sales.Api.Borders.Entities
{
    public class Salesman
    {
        [Key]
        public Guid Id { get; set; }
        public string DocumentNumber { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}