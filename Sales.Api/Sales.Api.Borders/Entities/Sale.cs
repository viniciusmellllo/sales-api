﻿using Sales.Api.Borders.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sales.Api.Borders.Entities
{
    public class Sale
    {
        [Key]
        public Guid Id { get; set; }
        public Salesman Salesman { get; set; }
        public List<Item> Items { get; set; }
        public SaleStatus Status { get; set; }
        public DateTime SoldAt { get; set; }
        public int OrderNumber { get; set; }
    }
}