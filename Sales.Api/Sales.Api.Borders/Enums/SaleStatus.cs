﻿namespace Sales.Api.Borders.Enums
{
    public enum SaleStatus
    {
        AcceptedPayment,
        CarrierSent,
        Delivered,
        Canceled
    }
}