﻿using Sales.Api.Borders.Entities;
using System;

namespace Sales.Api.Borders.Repositories
{
    public interface ISalesRepository
    {
        Sale GetSale(Guid saleId);
        Sale CreateSale(Sale sale);
        Sale UpdateSale(Sale sale);
    }
}