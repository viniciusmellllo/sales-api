﻿using Sales.Api.Borders.Dtos;
using Sales.Api.Borders.Entities;

namespace Sales.Api.Borders.UseCases
{
    public interface ICreateSaleUseCase
    {
        Sale Execute(SaleRequest saleRequest);
    }
}