﻿using Sales.Api.Borders.Entities;
using System;

namespace Sales.Api.Borders.UseCases
{
    public interface IUpdateSaleUseCase
    {
        Sale Execute(Guid saleId, Sale sale);
    }
}