﻿using Sales.Api.Borders.Entities;
using System;

namespace Sales.Api.Borders.UseCases
{
    public interface IGetSaleUseCase
    {
        Sale Execute(Guid saleId);
    }
}