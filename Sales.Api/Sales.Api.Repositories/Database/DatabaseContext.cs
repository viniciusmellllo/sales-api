﻿using Microsoft.EntityFrameworkCore;
using Sales.Api.Borders.Entities;

namespace Sales.Api.Repositories.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        public DbSet<Sale> Sales { get; set; }
    }
}