﻿using Sales.Api.Borders.Entities;
using Sales.Api.Borders.Repositories;
using Sales.Api.Repositories.Database;
using System;
using System.Linq;

namespace Sales.Api.Repositories
{
    public class SalesRepository : ISalesRepository
    {
        private readonly DatabaseContext _context;

        public SalesRepository(DatabaseContext context)
        {
            this._context = context;
        }

        public Sale GetSale(Guid saleId)
        {
            return _context.Sales.FirstOrDefault(s => s.Id == saleId);
        }

        public Sale CreateSale(Sale sale)
        {
            _context.Sales.Add(sale);
            _context.SaveChanges();
            return sale;
        }

        public Sale UpdateSale(Sale sale)
        {
            _context.SaveChanges();
            return sale;
        }
    }
}