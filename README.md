# README #

To run this .NET Core API => run Sales.Api package on IIS Express.

### What is this repository for? ###

* Sales API using EF InMemory for NAVA (Get, Create, Update methods)
* Version 0.1
* Postman collection for testing

### How do I get set up? ###

* Controller => UseCases => Repositories
* Configuration and Dependency Injection on Startup.cs

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin